import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

// Responsible for taking the interpreted word from voice recognition library and performing specific functions
public class functions {

	private Runtime runTime;
	private Robot simulate;
	private ArrayList<String> words;
	
	
	public functions (){
		runTime = Runtime.getRuntime();
		
		try {
			simulate = new Robot();
		} catch (AWTException e) {
			
			e.printStackTrace();
		}
		
		words = new ArrayList<String>();
	}
	
	// Processes the word interpreted by voice recognition library
	public void acceptOrder(String c) throws InterruptedException{
	
	
		try {
			// opens firefox
			if( c.equals("one")){
			runTime.exec("C:/Program Files (x86)/Mozilla Firefox/firefox.exe");
			}
			
			// closes any current application/window selected
			if (c.equals("finish")){
				simulate.keyPress(KeyEvent.VK_ALT);
				simulate.keyPress(KeyEvent.VK_F4);
				simulate.keyRelease(KeyEvent.VK_ALT);
				simulate.keyRelease(KeyEvent.VK_F4);
			}
			
			//puts comp in sleep mode
			if( c.equals("sleep")){
				runTime.exec("C:/Users/Cheiffa/Documents/sleep.bat");
			}
			
			// deletes a file/folder 
			if(c.equals("two")){
				simulate.keyPress(KeyEvent.VK_DELETE);
				simulate.keyRelease(KeyEvent.VK_DELETE);
				Thread.sleep(200);
				
				simulate.keyPress(KeyEvent.VK_ENTER);
				simulate.keyRelease(KeyEvent.VK_ENTER);
			}
			
			// takes explorer/internet browsers to previous page
			if(c.equals("seven")){
				simulate.keyPress(KeyEvent.VK_ALT);
				simulate.keyPress(KeyEvent.VK_LEFT);
				Thread.sleep(200);
				
				simulate.keyRelease(KeyEvent.VK_ALT);
				simulate.keyRelease(KeyEvent.VK_LEFT);
			}
			
			// takes explorer/internet browsers to next page
			if(c.equals("four")){
				simulate.keyPress(KeyEvent.VK_ALT);
				simulate.keyPress(KeyEvent.VK_RIGHT);
				Thread.sleep(200);
				
				simulate.keyRelease(KeyEvent.VK_ALT);
				simulate.keyRelease(KeyEvent.VK_RIGHT);
			}
			
			// copies the file/folder
			if(c.equals("c")){
				simulate.keyPress(KeyEvent.VK_CONTROL);
				simulate.keyPress(KeyEvent.VK_C);
				Thread.sleep(200);
				simulate.keyRelease(KeyEvent.VK_CONTROL);
				simulate.keyRelease(KeyEvent.VK_C);

			}
			
			// pastes the file/folder
			if(c.equals("g")){
				simulate.keyPress(KeyEvent.VK_CONTROL);
				simulate.keyPress(KeyEvent.VK_V);
				Thread.sleep(200);
				simulate.keyRelease(KeyEvent.VK_CONTROL);
				simulate.keyRelease(KeyEvent.VK_V);
			

			}
			// Puts in the password when at login screen
			if(c.equals("ten")){
				readFromFile();
				simulate.keyPress(KeyEvent.VK_CONTROL);
				simulate.keyPress(KeyEvent.VK_V);
				Thread.sleep(200);
				simulate.keyRelease(KeyEvent.VK_CONTROL);
				simulate.keyRelease(KeyEvent.VK_V);
			    
				simulate.keyPress(KeyEvent.VK_ENTER);
				simulate.keyRelease(KeyEvent.VK_ENTER);

			}
			
			
			
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	}
	
	private void readFromFile(){
		Scanner in=null;
		
		try {
			in = new Scanner(new FileReader("C:/Users/Cheiffa/Downloads/docs/key.txt"));
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
		}
		
		String word="";
		
		while(in.hasNext()){
			word=word+in.next();
		}
		
		System.out.println(word);
		
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
	    StringSelection stringSelection = new StringSelection( word );
	    clipboard.setContents(stringSelection, stringSelection);
		
	}
	

}
