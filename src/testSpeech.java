

import edu.cmu.sphinx.api.Configuration;
import edu.cmu.sphinx.api.LiveSpeechRecognizer;


// Responsible for mainly processing sound queues and sending the interpreted word to functions class for specific actions
public class testSpeech {       
	
	

                                     
    public static void main(String[] args) throws Exception {
                             
    	functions command= new functions();
    	
        Configuration configuration = new Configuration();

        configuration
                .setAcousticModelPath("resource:/edu/cmu/sphinx/models/en-us/en-us");
        configuration
                .setDictionaryPath("resource:/edu/cmu/sphinx/models/en-us/cmudict-en-us.dict");
        configuration
                .setLanguageModelPath("resource:/edu/cmu/sphinx/models/en-us/en-us.lm.bin");

        LiveSpeechRecognizer recognizer = new LiveSpeechRecognizer(configuration);
        
        
        recognizer.startRecognition(true);
       
        while (true) {
            String utterance = recognizer.getResult().getHypothesis();
            if (utterance.equals("end"))
                break;
            else
                System.out.println(utterance);
            
            command.acceptOrder(utterance);
          
        }
        recognizer.stopRecognition();
      
    }
}